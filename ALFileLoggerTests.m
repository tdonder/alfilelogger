//
//  ALFileLoggerTests.m
//  speeed
//
//  Created by Tobias Donder on 20.04.15.
//  Copyright (c) 2015 appleitung. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "ALFileLogger.h"

@interface ALFileLoggerTests : XCTestCase

@property (nonatomic) ALFileLogger *sut;
@property (nonatomic) NSString *textToLog;

@end

@implementation ALFileLoggerTests

- (void)setUp {
    [super setUp];
    
    self.textToLog = @"I am the text that should be logged to a file.";
    self.sut = [[ALFileLogger alloc] initWithLogFilePath:[NSString stringWithFormat:@"%@/%@", NSTemporaryDirectory(), @"loggedThings.txt"]];
}

- (void)tearDown {
    [self.sut resetLog];
    
    [super tearDown];
}

- (void)testLogAndLoad {
    [self.sut log:self.textToLog];
    
    NSString *returnedLogContent = self.sut.loggedContent;
    
    XCTAssertTrue([returnedLogContent containsString:self.textToLog]);
}

- (void)testLogFailsForInvalidLogFile {
    ALFileLogger *failingFileLogger = [[ALFileLogger alloc] initWithLogFilePath:@"/invalidPath/invalidLogFile.txt"];
    
    XCTAssertFalse([failingFileLogger log:@"testMessage"]);
}

@end
