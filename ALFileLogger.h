//
//  ALFileLogger.h
//  speeed
//
//  Created by Tobias Donder on 20.04.15.
//  Copyright (c) 2015 appleitung. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ALLog(message, ...) [[ALFileLogger new] log : (message), ## __VA_ARGS__]

@interface ALFileLogger : NSObject

/**
 *  Initializes FileLogger which logs to given file.
 *
 *  @param logFilePath The path to log messages to. If nil defaults to basic path.
 *
 *  @return The initialized logger.
 */
- (instancetype)initWithLogFilePath:(NSString *)logFilePath NS_DESIGNATED_INITIALIZER;

/**
*  Logs given message to text file.
*
*  @param message The message to log (can be a formatted string as per NSString).
*
*  @return YES, if logging successful, NO otherwise.
*/
- (BOOL)log:(NSString *)message, ...;

/**
 *  Returns the complete log content.
 *
 *  @return The logged content.
 */
- (NSString *)loggedContent;

/**
 *  Resets current log file.
 *
 *  @return Any error that might have occurred.
 */
- (NSError *)resetLog;

@end
