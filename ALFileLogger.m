//
//  ALFileLogger.m
//  speeed
//
//  Created by Tobias Donder on 20.04.15.
//  Copyright (c) 2015 appleitung. All rights reserved.
//

#import "ALFileLogger.h"

@interface ALFileLogger ()

@property (nonatomic, copy) NSString *logFilePath;

@end

@implementation ALFileLogger

- (instancetype)initWithLogFilePath:(NSString *)logFilePath
{
    self = [super init];
    
    if (self && logFilePath)
        _logFilePath = logFilePath;
    
    return self;
}

- (BOOL)log:(NSString *)message, ...
{
    va_list args;
    
    if (args) {
        va_start(args, message);
        message = [[NSString alloc] initWithFormat:message arguments:args];
        va_end(args);
    }
    
    NSError *fileError;

    if (![[NSFileManager defaultManager] fileExistsAtPath:self.logFilePath] &&
        ![[NSFileManager defaultManager] createFileAtPath:self.logFilePath contents:nil attributes:nil]) {
        return NO;
    }
    
    NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:self.logFilePath];
    [file seekToEndOfFile];
    [file writeData:[[ALFileLogger FormatMessage:message] dataUsingEncoding:NSUTF8StringEncoding]];
    [file closeFile];
    
    return fileError != nil;
}

- (NSString *)loggedContent
{
    return [[NSMutableString alloc] initWithContentsOfFile:self.logFilePath encoding:NSUTF8StringEncoding error:nil];
}

- (NSError *)resetLog
{
    NSError *fileError;
    [[NSFileManager defaultManager] removeItemAtPath:self.logFilePath error:&fileError];
    
    return fileError;
}

#pragma mark - Properties
- (NSString *)logFilePath
{
    if (!_logFilePath)
        _logFilePath = [ALFileLogger LogFilePath];
    
    return _logFilePath;
}

#pragma mark - Privates
+ (NSString *)LogFilePath
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = [documentsDirectory stringByAppendingPathComponent:@"speetLog.txt"];
    
    return fileName;
}

+ (NSString *)FormatMessage:(NSString *)message
{
    return [NSString stringWithFormat:@"%@ - %@\n", [[ALFileLogger LogFileDateFormatter] stringFromDate:[NSDate date]], message];
}

+ (NSDateFormatter *)LogFileDateFormatter
{
    NSMutableDictionary *dictionary = [[NSThread currentThread]
                                       threadDictionary];
    NSString *dateFormat = @"yyyy'-'MM'-'dd' 'HH':'mm'";
    NSString *key = [NSString stringWithFormat:@"logFileDateFormatter.%@", dateFormat];
    NSDateFormatter *dateFormatter = dictionary[key];
    
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:dateFormat];
        dictionary[key] = dateFormatter;
    }
    
    return dateFormatter;
}

@end
